// ===========================================================================================
// ---------------------------------------Get Machine Names-----------------------------------
// ===========================================================================================

function getMachines() {
  fetch("https://datafleet.io:8443/app-tools/plates/groups")
    .then((Response) => Response.json())
    .then((data) => {
      const machines = data.groups;

      // Maquinas Models
      const maquinasBtn = document.getElementById("maquinasBtn");
      maquinasBtn.addEventListener("click", () => {
        const models = document.querySelector("[models]");
        models.innerHTML = "";
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "MÁQUINAS") {
            const li = document.createElement("li");
            li.innerText = model;
            models.appendChild(li);
          }
        });
      });

      const addMaquinas = document.getElementById("addMaquinas");
      addMaquinas.addEventListener("click", () => {
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "MÁQUINAS") {
            const select = document.getElementById("equipamentos");
            const option = document.createElement("option");

            option.value = model;
            option.innerText = model;

            select.appendChild(option);
          }
        });
      });

      // Caminhoes Models
      const caminhoesBtn = document.getElementById("caminhoesBtn");
      caminhoesBtn.addEventListener("click", () => {
        const models = document.querySelector("[models]");
        models.innerHTML = "";
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "CAMINHÕES") {
            const li = document.createElement("li");
            li.innerText = model;
            models.appendChild(li);
          }
        });
      });

      const addCaminhoes = document.getElementById("addCaminhoes");
      addCaminhoes.addEventListener("click", () => {
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "CAMINHÕES") {
            const select = document.getElementById("equipamentos");
            const option = document.createElement("option");

            option.value = model;
            option.innerText = model;

            select.appendChild(option);
          }
        });
      });

      // Equipamentos Models
      const equipamentosBtn = document.getElementById("equipamentosBtn");
      equipamentosBtn.addEventListener("click", () => {
        const models = document.querySelector("[models]");
        models.innerHTML = "";
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "EQUIPAMENTOS") {
            const li = document.createElement("li");
            li.innerText = model;
            models.appendChild(li);
          }
        });
      });

      const addEquipamentos = document.getElementById("addEquipamentos");
      addEquipamentos.addEventListener("click", () => {
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "EQUIPAMENTOS") {
            const select = document.getElementById("equipamentos");
            const option = document.createElement("option");

            option.value = model;
            option.innerText = model;

            select.appendChild(option);
          }
        });
      });

      // Carretas Models
      const carretaBtn = document.getElementById("carretaBtn");
      carretaBtn.addEventListener("click", () => {
        const models = document.querySelector("[models]");
        models.innerHTML = "";
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "CARRETA") {
            const li = document.createElement("li");
            li.innerText = model;
            models.appendChild(li);
          }
        });
      });

      const addCarretas = document.getElementById("addCarretas");
      addCarretas.addEventListener("click", () => {
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "CARRETA") {
            const select = document.getElementById("equipamentos");
            const option = document.createElement("option");

            option.value = model;
            option.innerText = model;

            select.appendChild(option);
          }
        });
      });

      // Vans Models
      const vanBtn = document.getElementById("vanBtn");
      vanBtn.addEventListener("click", () => {
        const models = document.querySelector("[models]");
        models.innerHTML = "";
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "VAN") {
            const li = document.createElement("li");
            li.innerText = model;
            models.appendChild(li);
          }
        });
      });

      const addVan = document.getElementById("addVan");
      addVan.addEventListener("click", () => {
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "VAN") {
            const select = document.getElementById("equipamentos");
            const option = document.createElement("option");

            option.value = model;
            option.innerText = model;

            select.appendChild(option);
          }
        });
      });

      // 4x4 Models
      const quatroxquatroBtn = document.getElementById("utilitario-4x4Btn");
      quatroxquatroBtn.addEventListener("click", () => {
        const models = document.querySelector("[models]");
        models.innerHTML = "";
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "UTILITÁRIO 4x4") {
            const li = document.createElement("li");
            li.innerText = model;
            models.appendChild(li);
          }
        });
      });

      const add4x4 = document.getElementById("add4x4");
      add4x4.addEventListener("click", () => {
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "UTILITÁRIO 4x4") {
            const select = document.getElementById("equipamentos");
            const option = document.createElement("option");

            option.value = model;
            option.innerText = model;

            select.appendChild(option);
          }
        });
      });

      // Utilitario Leve Models
      const utilitarioLeveBtn = document.getElementById("utilitario-leveBtn");
      utilitarioLeveBtn.addEventListener("click", () => {
        const models = document.querySelector("[models]");
        models.innerHTML = "";
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "UTILITÁRIO LEVE") {
            const li = document.createElement("li");
            li.innerText = model;
            models.appendChild(li);
          }
        });
      });

      const addUtilitarioLeve = document.getElementById("addUtilitarioLeve");
      addUtilitarioLeve.addEventListener("click", () => {
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "UTILITÁRIO LEVE") {
            const select = document.getElementById("equipamentos");
            const option = document.createElement("option");

            option.value = model;
            option.innerText = model;

            select.appendChild(option);
          }
        });
      });

      // Executivo Models
      const executivoBtn = document.getElementById("executivoBtn");
      executivoBtn.addEventListener("click", () => {
        const models = document.querySelector("[models]");
        models.innerHTML = "";
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "EXECUTIVO") {
            const li = document.createElement("li");
            li.innerText = model;
            models.appendChild(li);
          }
        });
      });

      const addExecutivo = document.getElementById("addExecutivo");
      addExecutivo.addEventListener("click", () => {
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "EXECUTIVO") {
            const select = document.getElementById("equipamentos");
            const option = document.createElement("option");

            option.value = model;
            option.innerText = model;

            select.appendChild(option);
          }
        });
      });

      // Passeio Models
      const passeioBtn = document.getElementById("passeioBtn");
      passeioBtn.addEventListener("click", () => {
        const models = document.querySelector("[models]");
        models.innerHTML = "";
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "PASSEIO") {
            const li = document.createElement("li");
            li.innerText = model;
            models.appendChild(li);
          }
        });
      });

      const addPasseio = document.getElementById("addPasseio");
      addPasseio.addEventListener("click", () => {
        machines.forEach((machine) => {
          const group = machine.group;
          const model = machine.model;
          if (group == "PASSEIO") {
            const select = document.getElementById("equipamentos");
            const option = document.createElement("option");

            option.value = model;
            option.innerText = model;

            select.appendChild(option);
          }
        });
      });

      // machines.forEach((machine) => {
      //   const group = machine.group;
      //   const model = machine.model;
      //   if (group == "MÁQUINAS") {
      //     // Select Models
      //     const select = document.getElementById("maquinas");
      //     const option = document.createElement("option");

      //     option.value = model;
      //     option.innerText = model;

      //     select.appendChild(option);
      //   } else if (group == "CAMINHÕES") {
      //     const select = document.getElementById("caminhoes");
      //     const option = document.createElement("option");

      //     option.value = model;
      //     option.innerText = model;

      //     select.appendChild(option);
      //   } else if (group == "EQUIPAMENTOS") {
      //     const select = document.getElementById("equipamentos");
      //     const option = document.createElement("option");

      //     option.value = model;
      //     option.innerText = model;

      //     select.appendChild(option);
      //   } else if (group == "CARRETA") {
      //     const select = document.getElementById("carreta");
      //     const option = document.createElement("option");

      //     option.value = model;
      //     option.innerText = model;

      //     select.appendChild(option);
      //   } else if (group == "VAN") {
      //     const select = document.getElementById("van");
      //     const option = document.createElement("option");

      //     option.value = model;
      //     option.innerText = model;

      //     select.appendChild(option);
      //   } else if (group == "UTILITÁRIO 4x4") {
      //     const select = document.getElementById("utilitario-4x4");
      //     const option = document.createElement("option");

      //     option.value = model;
      //     option.innerText = model;

      //     select.appendChild(option);
      //   } else if (group == "UTILITÁRIO LEVE") {
      //     const select = document.getElementById("utilitario-leve");
      //     const option = document.createElement("option");

      //     option.value = model;
      //     option.innerText = model;

      //     select.appendChild(option);
      //   } else if (group == "EXECUTIVO") {
      //     const select = document.getElementById("executivo");
      //     const option = document.createElement("option");

      //     option.value = model;
      //     option.innerText = model;

      //     select.appendChild(option);
      //   } else if (group == "PASSEIO") {
      //     const select = document.getElementById("passeio");
      //     const option = document.createElement("option");

      //     option.value = model;
      //     option.innerText = model;

      //     select.appendChild(option);
      //   }
      // });
    });
}

getMachines();
